﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
    public enum CardSuit
    {
        DIAMOND = 1,
        CLUBS,
        HEARTS,
        SPADES
    }

    //represents a card game playering card

    public class Card
    {
        private byte _value;

        private CardSuit _suit;
    }
}
